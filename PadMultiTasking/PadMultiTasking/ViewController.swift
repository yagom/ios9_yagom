//
//  ViewController.swift
//  PadMultiTasking
//
//  Created by yagom on 2015. 8. 20..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class ViewController: UIViewController, AVPlayerViewControllerDelegate {
    
    // MARK:- IBAction
    @IBAction func clickPlayVideo() {
        
        // 비디오를 재생할 비디오 재생 뷰컨트롤러
        let videoController: AVPlayerViewController = AVPlayerViewController()
        videoController.delegate = self
        
        // 비디오파일은 프로젝트에 포함되어있지 않습니다. 비디오파일은 직접 프로젝트에 넣어주세요.
        // Mac 기본내장 QuickTime Player로 동영상 만들기 : https://support.apple.com/ko-kr/HT201066
        guard let path = NSBundle.mainBundle().pathForResource("movie", ofType: "mov") else {
            return
        }
        
        // 비디오 파일 URL을 사용하여 플레이어를 생성
        let url = NSURL(fileURLWithPath: path)
        videoController.player = AVPlayer(URL: url)
        
        // 화면에 보여줍니다.
        self.presentViewController(videoController, animated: true, completion: nil)
    }
    
    
    // MARK:- AVPlayerViewControllerDelegate
    // 픽쳐인픽쳐 모드로 진입시 플레이어 뷰 컨트롤러를 Dismiss 할 것인가를 판단하는데에 대한 델리게이트 메소드입니다.
    func playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart(playerViewController: AVPlayerViewController) -> Bool {
        return false
    }
    
/*  그 외 AVPlayerViewControllerDelegate의 다양한 델리게이트 메소드들
    func playerViewControllerWillStartPictureInPicture(playerViewController: AVPlayerViewController) {
        
    }
    
    func playerViewControllerWillStopPictureInPicture(playerViewController: AVPlayerViewController) {
        
    }
    func playerViewController(playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: NSError) {
    }
    
    func playerViewController(playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: (Bool) -> Void) {
        
    }
    func playerViewControllerDidStartPictureInPicture(playerViewController: AVPlayerViewController) {
        
    }
    
    func playerViewControllerDidStopPictureInPicture(playerViewController: AVPlayerViewController) {
        
    }
*/
}

