//
//  DataManager.swift
//  WatchOSWatchKit
//
//  Created by yagom on 2015. 9. 15..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import WatchConnectivity

// NSUserDefaultsDidChangeNotification
//let DidUserDefaultChangedNotification = "userDefaultChange"

// User Defaults에 저장될 배열의 키입니다.
let KeyInfoArray: String = "InfoArr"

// 기존 NSUserDefaults 클래스에 extension을 통해 메소드를 추가해줍니다.
extension NSUserDefaults {
    
    // NSUserDefaults에서 배열을 가져옵니다.
    // 만난 시각들을 저장하고 있는 배열입니다.
    func infoArray(inout arr: [NSDate]?) {
        
        // 기존에 배열이 존재한다면
        if let infoArr = self.objectForKey(KeyInfoArray) as? [NSDate] {
            arr = infoArr
        } else {
            // 그렇지 않다면 새로 만들어줍니다.
            arr = [NSDate]()
            
            if let realArr = arr {
                self.setObject(realArr, forKey: KeyInfoArray)
            }
        }
        self.synchronize()
    }
    
    // 만난 시각을 추가합니다.
    func addNewDate(inout arr: [NSDate]?) {
        arr?.append(NSDate())
        self.setObject(arr, forKey: KeyInfoArray)
        self.synchronize()
        
        // Watch Connectivity를 사용하여 보낼 UserInfo를 생성합니다.
        var userInfoDic: [String: AnyObject] = [String: AnyObject]()
        userInfoDic[KeyInfoArray] = arr
        
        // 세션을 사용하여 UserInfo 딕셔너리를 전송합니다.
        WCSession.defaultSession().transferUserInfo(userInfoDic)
    }
}

// Connectivity를 통해 전달받은 UserInfo의 정보를 워치의 로컬 User Defaults에 저장합니다.
func saveInfoFromUserInfo(userInfo: [String: AnyObject]) {

    let userDefault: NSUserDefaults? = NSUserDefaults.standardUserDefaults()
    
    for (key, value) in userInfo {
        userDefault?.setObject(value, forKey: key)
    }
    
    userDefault?.synchronize()
}
