//
//  AppDelegate.swift
//  WatchOSWatchKit
//
//  Created by yagom on 2015. 9. 15..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WCSessionDelegate {
    
    var window: UIWindow?
    
    // MARK:- Life Cycle
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Watch Connectivity를 지원하는지 확인하여 델리케이트를 설정해주고, 세션을 활성화합니다.
        if (WCSession.isSupported()) {
            WCSession.defaultSession().delegate = self
            WCSession.defaultSession().activateSession()
        }
        
        return true
    }
    
    //MARK:- Watch Connectivity Session Delegate
    
    // 워치로부터 UserInfo를 수신했을 때 호출됩니다.
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        print("didReceiveUserInfo \(userInfo)")
        saveInfoFromUserInfo(userInfo)
    }

    // 워치로 UserInfo를 송신했을 때 호출됩니다.
    func session(session: WCSession, didFinishUserInfoTransfer userInfoTransfer: WCSessionUserInfoTransfer, error: NSError?) {
        print("didFinishUserInfoTransfer \(error)")
    }
    
    /*
    // Application Context와 Message 관련 델리게이트 메소드입니다.
    
    func session(session: WCSession, didReceiveApplicationContext applicationContext: [String : AnyObject]) {
        print("Recieve context")
    }
    
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject]) {
        print("Recieve didReceiveMessage")
    }
    */

}

