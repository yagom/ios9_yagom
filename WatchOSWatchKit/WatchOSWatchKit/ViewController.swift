//
//  ViewController.swift
//  WatchOSWatchKit
//
//  Created by yagom on 2015. 9. 15..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import WatchConnectivity

// 시간을 리스트로 표현하고 싶기 때문에 테이블뷰 컨트롤러를 사용하였습니다.
class ViewController: UITableViewController {
    
    // MARK:- Properties
    var userDefault: NSUserDefaults?
    var infoArr: [NSDate]?
    var dateFormatter: NSDateFormatter?
    
    
    // MARK:- IBAction
    @IBAction func clickPlusButton() {
        self.userDefault?.addNewDate(&self.infoArr)
    }
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // User Defaults 가 변경되었음을 알리는 노티피케이션을 수신받습니다.
        // User Info를 수신받아 User Defaults가 변경되면 setViewData 메소드를 통해 화면이 새로고침 됩니다.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshView", name: NSUserDefaultsDidChangeNotification, object: nil)
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter?.dateFormat = "yyyy.MM.dd - hh:mm:ss"
        self.dateFormatter?.locale = NSLocale.currentLocale()
        
        self.refreshView()
    }
    
    // MARK:- Setting Views
    func refreshView() {
        self.userDefault = NSUserDefaults.standardUserDefaults()
        self.userDefault?.infoArray(&self.infoArr)
        
        // 뷰 컨트롤러의 제목을 지정해줍니다.
        if let count = self.infoArr?.count {
            self.title = "Count : \(count)"
        }
        
        self.tableView.reloadData()
    }
    
    // MARK:- TableView Data Source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.infoArr?.count {
            return count
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("InfoCell")
        
        if let date: NSDate = infoArr?[indexPath.row] {
            cell?.textLabel?.text = self.dateFormatter?.stringFromDate(date)
        }
        
        return cell!
    }
}


