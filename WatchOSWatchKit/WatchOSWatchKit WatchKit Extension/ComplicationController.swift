//
//  ComplicationController.swift
//  WatchOSWatchKit WatchKit Extension
//
//  Created by yagom on 2015. 9. 15..
//  Copyright © 2015년 yagom. All rights reserved.
//

import ClockKit

// 워치애플리케이션의 컴플리케이션 화면을 컨트롤할 클래스입니다.
class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK:- Properties
    var userDefault: NSUserDefaults?
    var infoArr: [NSDate]?
    var dateFormatter: NSDateFormatter?
    var timeFormatter: NSDateFormatter?
    
    // MARK:- Complication Data Source
    // Complication의 데이터 업데이트 요청시 호출됩니다.
    func requestedUpdateDidBegin() {
        self.userDefault = NSUserDefaults.standardUserDefaults()
        self.userDefault?.infoArray(&self.infoArr)
        
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter?.dateFormat = "yy.MM.dd"
        self.dateFormatter?.locale = NSLocale.currentLocale()
        
        
        self.timeFormatter = NSDateFormatter()
        self.timeFormatter?.dateFormat = "hh:mm:ss"
        self.timeFormatter?.locale = NSLocale.currentLocale()
        
    }
    
    // MARK: Timeline Configuration
    // 타임 트래블 기능을 어떻게 지원할지 핸들러를 통해 반환합니다.
    // CLKComplicationTimeTravelDirections에는 Forward, Backward, None이 있습니다.
    func getSupportedTimeTravelDirectionsForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTimeTravelDirections) -> Void) {
        
        handler([.Forward, .Backward])
    }
    
    // 컴플리케이션의 타임트래블 시작시각을 돌려줍니다.
    func getTimelineStartDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        
        handler(self.infoArr?.first)
    }
    
    // 컴플리케이션의 타임트래블 종료시각을 돌려줍니다.
    func getTimelineEndDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        
        handler(self.infoArr?.last)
    }
    
    // 워치가 비밀번호잠금 상태일 때 화면에 표시할지 결정합니다.
    func getPrivacyBehaviorForComplication(complication: CLKComplication, withHandler handler:
        (CLKComplicationPrivacyBehavior) -> Void) {
            
            // 비밀번호 잠금화면에서 노출합니다.
            // 개인적인 정보라면 HideOnLockScreen 옵션을 사용합니다.
            handler(.ShowOnLockScreen)
    }
    
    // MARK: Timeline Population
    // 현재시각 화면에 표시되어야 할 엔트리를 반환합니다.
    func getCurrentTimelineEntryForComplication(complication: CLKComplication, withHandler handler: ((CLKComplicationTimelineEntry?) -> Void)) {

        // 최근 자료를 통해 템플릿을 생성하여 핸들러로 엔트리를 전달합니다.
        if let arr = self.infoArr, lastDate = arr.last, template = self.clockTemplate(complication) {
            let entry: CLKComplicationTimelineEntry = CLKComplicationTimelineEntry(date: lastDate, complicationTemplate: template)
            handler(entry)
        }
        
        // 전송할 자료가 없다면 nil을 반환합니다.
        handler(nil)
    }
    
    // 타임트래블 기능을 이용하여 특정 시간 이전 시간별 엔트리를 반환합니다.
    func getTimelineEntriesForComplication(complication: CLKComplication, beforeDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {

        var entries: [CLKComplicationTimelineEntry] = [CLKComplicationTimelineEntry]()

        if let arr = self.infoArr {
            for dt in arr {
                
                if dt.timeIntervalSinceDate(date) >= 0 {
                    continue
                }
                
                if let template = self.clockTemplate(complication, date: dt) {
                    
                    let entry: CLKComplicationTimelineEntry = CLKComplicationTimelineEntry(date: dt, complicationTemplate: template)
                    entries.append(entry)
                    
                    if entries.count >= limit {
                        break
                    }
                }
            }
        }
        
        handler(entries)
    }
    
    // 타임트래블 기능을 이용하여 특정 시간 이후 시간별 엔트리를 반환합니다.
    func getTimelineEntriesForComplication(complication: CLKComplication, afterDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
       
        handler(nil)
    }
    
    // MARK: Update Scheduling
    
    // 이후 한 번 데이터 업데이트 할 기회를 주게되는데, 그 시간을 지정합니다.
    // 업데이트를 보장하지는 않습니다.
    func getNextRequestedUpdateDateWithHandler(handler: (NSDate?) -> Void) {
        // 10분 후 업데이트 요청
        handler(NSDate(timeIntervalSinceNow: 600));
    }
    
    // MARK: Placeholder Templates
    // 컴플리케이션 미리보기화면으로 사용될 템플릿을 반환합니다.
    // 워치 애플리케이션 설치 후 딱 한 번 만 호출되며, 캐시됩니다.
    func getPlaceholderTemplateForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTemplate?) -> Void) {
        
        let template: CLKComplicationTemplate? = self.clockTemplate(complication)
        
        handler(template)
    }
    
    // MARK:- Custom Template
    
    // 컴플리케이션 템플릿을 컴플리케이션의 종류에 따라 생성하여 반환합니다.
    func clockTemplate(complication: CLKComplication) -> CLKComplicationTemplate? {
        return self.clockTemplate(complication, date: nil)
    }
    
    // 컴플리케이션 템플릿을 컴플리케이션의 종류와 시각에 따라 생성하여 반환합니다.
    func clockTemplate(complication: CLKComplication, date: NSDate?) -> CLKComplicationTemplate? {
        
        // 정보가 없으면 nil을 반환합니다.
        guard let arr = self.infoArr else {
            return nil
        }
        
        // 정보가 없으면 nil을 반환합니다.
        if arr.count == 0 {
            return nil
        }

        // ModularLarge 모드에서 화면아래에 표시될 날짜입니다.
        let lastDate: NSDate
        
        if let dt = date {
            lastDate = dt
        } else {
            lastDate = arr.last!
        }
        
        // ModularLarge 모드에서 화면위에 표시될 날짜입니다.
        let firstDate: NSDate
        
        if lastDate == arr.first {
            firstDate = lastDate
        } else {
            firstDate = arr[arr.indexOf(lastDate)! - 1]
        }
        
        // 해당 날짜까지의 총 카운트
        let count: Int = arr.indexOf(lastDate)! + 1
        
        // 컴플리케이션의 종류에 따라 다른 템플릿을 사용합니다.
        // 적절히 구성된 템플릿을 반환하여줍니다.
        switch complication.family {
            
        case .CircularSmall :
            
            let template: CLKComplicationTemplateCircularSmallSimpleText = CLKComplicationTemplateCircularSmallSimpleText()
            template.textProvider = CLKSimpleTextProvider(text: "\(count)")
            return template
            
        case .ModularLarge :
            
            let template: CLKComplicationTemplateModularLargeTable = CLKComplicationTemplateModularLargeTable()
            template.headerTextProvider = CLKSimpleTextProvider(text: "Count : \(count)", shortText: "\(count)")
            template.row1Column1TextProvider = CLKDateTextProvider(date: firstDate, units: [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day])
            template.row1Column2TextProvider = CLKTimeTextProvider(date: firstDate)
            template.row2Column1TextProvider = CLKDateTextProvider(date: lastDate, units: [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day])
            template.row2Column2TextProvider = CLKTimeTextProvider(date: lastDate)
            
            return template
            
        case .ModularSmall :
            
            let template: CLKComplicationTemplateModularSmallSimpleText = CLKComplicationTemplateModularSmallSimpleText()
            template.textProvider = CLKSimpleTextProvider(text: "\(count)")
            
            return template
            
        case .UtilitarianLarge :
            
            let template: CLKComplicationTemplateUtilitarianLargeFlat = CLKComplicationTemplateUtilitarianLargeFlat()
            template.textProvider = CLKSimpleTextProvider(text: "Count : \(count)", shortText: "\(count)")
            return template
            
        case .UtilitarianSmall :
            
            let template: CLKComplicationTemplateUtilitarianSmallFlat = CLKComplicationTemplateUtilitarianSmallFlat()
            template.textProvider = CLKSimpleTextProvider(text: "Cnt : \(count)", shortText: "\(count)")
            return template
            
        }
    }    
}
