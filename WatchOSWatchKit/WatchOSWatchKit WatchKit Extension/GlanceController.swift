//
//  GlanceController.swift
//  WatchOSWatchKit WatchKit Extension
//
//  Created by yagom on 2015. 9. 15..
//  Copyright © 2015년 yagom. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

// 워치 애플리케이션의 한 눈에 보기 화면이 될 클래스입니다.
class GlanceController: WKInterfaceController, WCSessionDelegate {
    
    // MARK:- Properties
    var userDefault: NSUserDefaults?
    var infoArr: [NSDate]?
    var dateFormatter: NSDateFormatter?
    
    @IBOutlet var countLabel: WKInterfaceLabel?
    @IBOutlet var table: WKInterfaceTable?
    
    // MARK:- Life Cycle
    // UIViewControlelr의 awakeFromNib과 유사한 메소드입니다.
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setViewData", name: NSUserDefaultsDidChangeNotification, object: nil)
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter?.dateFormat = "yy.MM.dd - hh:mm"
        self.dateFormatter?.locale = NSLocale.currentLocale()
        
    }
    // 한 눈에 보기 화면이 활성화 되기 직전에 호출됩니다.
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.setViewData()
    }
    
    // MARK:- Setting Views
    // 화면에 보여질 데이터를 처리하고 세팅합니다.
    func setViewData() {
        
        // Watch Connectivity를 지원하는지 확인하여 델리케이트를 설정해주고, 세션을 활성화합니다.
        if (WCSession.isSupported()) {
            WCSession.defaultSession().delegate = self
            WCSession.defaultSession().activateSession()
        }
        
        self.userDefault = NSUserDefaults.standardUserDefaults()
        self.userDefault?.infoArray(&self.infoArr)
        
        if let arr = self.infoArr {
            let count: Int = arr.count
            
            // WatchKit의 테이블은 델리게이트를 사용하지 않고 직접 데이터를 세팅해줍니다.
            self.table?.setNumberOfRows(2, withRowType: "RowController")
            
            self.countLabel?.setText("Count : \(count)")
            
            if count == 0 { return }
            
            for var i = 1; i < (count > 2 ? 3 : count) ; i++ {
                if let date: NSDate = arr[count - i] {
                    if let str = self.dateFormatter?.stringFromDate(date) {
                        let row = self.table?.rowControllerAtIndex(i-1) as? RowController
                        row?.rowLabel?.setText(str)
                    }
                }
            }
        }
        
    }
    
    // MARK:- Watch Connectivity Session Delegate
    
    // iOS로부터 UserInfo를 수신했을 때 호출됩니다.
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        saveInfoFromUserInfo(userInfo)
    }
    
    // iOS로 UserInfo를 송신했을 때 호출됩니다.
    func session(session: WCSession, didFinishUserInfoTransfer userInfoTransfer: WCSessionUserInfoTransfer, error: NSError?) {
        print("didFinishUserInfoTransfer \(error)")
    }
}
