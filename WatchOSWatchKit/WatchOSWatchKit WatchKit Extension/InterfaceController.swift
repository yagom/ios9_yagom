//
//  InterfaceController.swift
//  WatchOSWatchKit WatchKit Extension
//
//  Created by yagom on 2015. 9. 15..
//  Copyright © 2015년 yagom. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

// WKInterfaceTable의 Row를 표현해줄 컨트롤러입니다.
class RowController: NSObject {
    @IBOutlet weak var rowLabel: WKInterfaceLabel?
}

// 워치 애플리케이션의 메인 화면이 될 클래스입니다.
class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    // MARK:- Properties
    var userDefault: NSUserDefaults?
    var infoArr: [NSDate]?
    var dateFormatter: NSDateFormatter?
    
    @IBOutlet var countLabel: WKInterfaceLabel?
    @IBOutlet var table: WKInterfaceTable?
    
    // MARK:- IBAction
    @IBAction func clickAddDateButton() {
        self.userDefault?.addNewDate(&self.infoArr)
        self.setViewData()
    }
    
    // MARK:- Life Cycle
    // UIViewControlelr의 awakeFromNib과 유사한 메소드입니다.
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter?.dateFormat = "yy.MM.dd - hh:mm"
        self.dateFormatter?.locale = NSLocale.currentLocale()
        
        // User Defaults 가 변경되었음을 알리는 노티피케이션을 수신받습니다.
        // User Info를 수신받아 User Defaults가 변경되면 setViewData 메소드를 통해 화면이 새로고침 됩니다.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setViewData", name: NSUserDefaultsDidChangeNotification, object: nil)
        
    }
    
    // 애플리케이션이 활성화 되기 직전에 호출됩니다.
    override func willActivate() {
        super.willActivate()
        self.setViewData()
    }
    
    // 애플리케이션이 비활성화 된 후 호출됩니다.
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    // MARK:- Setting Views
    
    // 화면에 보여질 데이터를 처리하고 세팅합니다.
    func setViewData() {
        
        // Watch Connectivity를 지원하는지 확인하여 델리케이트를 설정해주고, 세션을 활성화합니다.
        if (WCSession.isSupported()) {
            WCSession.defaultSession().delegate = self
            WCSession.defaultSession().activateSession()
        }
        
        self.userDefault = NSUserDefaults.standardUserDefaults()
        self.userDefault?.infoArray(&self.infoArr)
        
        if let arr = self.infoArr {
            let count: Int = arr.count
            
            // WatchKit의 테이블은 델리게이트를 사용하지 않고 직접 데이터를 세팅해줍니다.
            self.table?.setNumberOfRows(count, withRowType: "RowController")
            
            self.countLabel?.setText("Count : \(count)")
            
            for var i = 1; i < count + 1 ; i++ {
                if let date: NSDate = arr[count - i] {
                    if let str = self.dateFormatter?.stringFromDate(date) {
                        let row = self.table?.rowControllerAtIndex(i-1) as? RowController
                        row?.rowLabel?.setText(str)
                    }
                }
            }
        }
    }
    
    // MARK:- Watch Connectivity Session Delegate
    
    // iOS로부터 UserInfo를 수신했을 때 호출됩니다.
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        saveInfoFromUserInfo(userInfo)
    }
    
    // iOS로 UserInfo를 송신했을 때 호출됩니다.
    func session(session: WCSession, didFinishUserInfoTransfer userInfoTransfer: WCSessionUserInfoTransfer, error: NSError?) {
        print("didFinishUserInfoTransfer \(error)")
    }
    
}