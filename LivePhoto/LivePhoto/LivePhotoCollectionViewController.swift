//
//  LivePhotoCollectionViewController.swift
//  LivePhoto
//
//  Created by yagom on 2015. 10. 28..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

// MARK: - Delegate Protocol
protocol LivePhotoCollectionViewControllerDelegate: NSObjectProtocol {
    func livePhotoCollectionViewDidSelectedLivePhoto(asset: PHAsset)
}

// MARK: - Class
class LivePhotoCollectionViewController: UICollectionViewController {

    // MARK: Properties
    weak var livePhotoCeollcetionViewControllerDelegate: LivePhotoCollectionViewControllerDelegate?
    
    private let reuseIdentifier: String = "LivePhotoCell"
    private let livePhotoViewTag: Int = 999
    private var livePhotosFetchResult: PHFetchResult?

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 콜렉션뷰의 셀 클래스를 등록합니다.
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: self.reuseIdentifier)
        
        // 사진을 가져올 때 사용할 Fetch 옵션입니다. mediaSubtype 속성이 Live Photo인 사진만 가져오도록 옵션을 줍니다.
        let option = PHFetchOptions()
        option.predicate = NSPredicate(format: "(mediaSubtype & %d) != 0", PHAssetMediaSubtype.PhotoLive.rawValue)
        
        // 사진을 가져온 결과물을 livePhotosFetchResult 속성에 할당합니다.
        self.livePhotosFetchResult = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: option)
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.livePhotosFetchResult?.count ?? 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(self.reuseIdentifier, forIndexPath: indexPath)
        
        let livePhotoViewInCell: PHLivePhotoView
        
        // 만약 셀에 livePhotoViewTag를 태그값으로 가지는 PHLivePhotoView 객체가 있다면 기존 사진을 비워주고
        if let livePhotoView = cell.viewWithTag(self.livePhotoViewTag) as? PHLivePhotoView {
            livePhotoView.livePhoto = nil
            livePhotoViewInCell = livePhotoView
        } else {
            // 만약 없다면 새로 생성하여 셀에 얹어줍니다.
            let livePhotoView: PHLivePhotoView = PHLivePhotoView(frame: CGRect(origin: CGPointZero, size: cell.frame.size))
            livePhotoView.tag = self.livePhotoViewTag
            cell.addSubview(livePhotoView)
            livePhotoViewInCell = livePhotoView
        }
        
        // 현재 셀의 인덱스에 해당하는 사진 에셋을 가져옵니다.
        guard let asset: PHAsset = self.livePhotosFetchResult?.objectAtIndex(indexPath.item) as? PHAsset else {
            return cell
        }
        
        // 싱글턴 이미지 매니저입니다.
        let manager: PHImageManager = PHImageManager.defaultManager()
        
        // 셀이 새로고침 되는 것이므로 기존에 Live Photo 요청이 있으면 취소해줍니다.
        // 셀의 태그가 0이 아니라는 뜻은 기존의 Live Photo 요청ID로 세팅되어 있음을 의미합니다.
        if cell.tag != 0 {
            manager.cancelImageRequest(PHImageRequestID(cell.tag))
        }
        
        // Live Photo를 가져올 때 사용할 요청 옵션입니다. 네트워크 사용(iCloud 사진을 가져올 때 필요하므로)을 허용해줍니다.
        let requestOption: PHLivePhotoRequestOptions = PHLivePhotoRequestOptions()
        requestOption.networkAccessAllowed = true
        
        // 이미지 매니저를 통해 에셋에 해당하는 Live Photo를 요청합니다.
        // 동시에 셀의 태그를 Live Photo 요청ID로 설정합니다.
        cell.tag = Int(manager.requestLivePhotoForAsset(asset, targetSize: cell.frame.size, contentMode: PHImageContentMode.AspectFit, options: requestOption, resultHandler: { (livePhoto: PHLivePhoto?, userInfo: [NSObject : AnyObject]?) -> Void in
            
            // 사진을 가져오면 해당 셀의 Live Photo View에 보여줍니다.
            livePhotoViewInCell.livePhoto = livePhoto
        }))
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    // 콜렉션뷰의 셀을 선택했을 때 호출되는 메소드입니다.
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        // 델리게이트 객체의 livePhotoCollectionViewDidSelectedLivePhoto 메소드를 호출합니다.
        if let asset = self.livePhotosFetchResult?.objectAtIndex(indexPath.item) as? PHAsset {
            self.livePhotoCeollcetionViewControllerDelegate?.livePhotoCollectionViewDidSelectedLivePhoto(asset)
        }
        
        // 이전 화면으로 돌아갑니다.
        self.navigationController?.popViewControllerAnimated(true)
        return false
    }
}
