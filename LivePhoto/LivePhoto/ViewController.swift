//
//  ViewController.swift
//  LivePhoto
//
//  Created by yagom on 2015. 10. 28..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import MobileCoreServices

class ViewController: UIViewController, PHLivePhotoViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LivePhotoCollectionViewControllerDelegate {
    
    // MARK: - Properties
    private lazy var livePhotoView: PHLivePhotoView  = PHLivePhotoView(frame: UIScreen.mainScreen().bounds)
    
    // MARK: - IBActions
    @IBAction func clickSelectFromPickerButton() {
        // 이미지 피커를 생성합니다.
        let picker: UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
        // 피커에 보여질 미디어 타입을 이미지와 Live Photo로 설정해줍니다.
        // kUTTypeImage는 필수항목이므로 Live Photo만 선별해서 가져올 수는 없습니다.
        picker.mediaTypes = [kUTTypeImage as String, kUTTypeLivePhoto as String]
        
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.livePhotoView.contentMode = UIViewContentMode.ScaleAspectFit
        self.livePhotoView.delegate = self
        self.view.addSubview(self.livePhotoView)
    }
    
    // MARK: - PHLivePhotoViewDelegate
    // Live Photo가 재생되기 직전에 호출됩니다.
    func livePhotoView(livePhotoView: PHLivePhotoView, willBeginPlaybackWithStyle playbackStyle: PHLivePhotoViewPlaybackStyle) {
        print("Live photo willBeginPlaybackWithStyle \(playbackStyle.rawValue)")
    }
    
    // Live Photo가 재생된 직후에 호출됩니다.
    func livePhotoView(livePhotoView: PHLivePhotoView, didEndPlaybackWithStyle playbackStyle: PHLivePhotoViewPlaybackStyle) {
        print("Live photo didEndPlaybackWithStyle \(playbackStyle.rawValue)")
    }
    
    // MARK: - UIImagePickerControllerDelegate
    // 이미지피커에서 사용자가 사진을 선택했을 때 호출됩니다.
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        // Live Photo가 선택되면 info 딕셔너리에 UIImagePickerControllerLivePhoto라는 키를 가지는 객체가 들어옵니다.
        guard let livePhoto: PHLivePhoto = info[UIImagePickerControllerLivePhoto] as? PHLivePhoto else {
            // 만약 선택된 이미지가 Live Photo가 아니라면 얼럿을 보여주고 다시 선택하도록 합니다.
            let alert: UIAlertController = UIAlertController(title: "알림", message: "Live Photo가 아닙니다. 다시 선택해주세요.", preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction: UIAlertAction = UIAlertAction(title: "확인", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                picker.popViewControllerAnimated(true)
            })
            alert.addAction(okAction)
            
            picker.presentViewController(alert, animated: true, completion: nil)
            
            return
        }
        
        // 화면의 Live Photo View에 해당 Live Photo를 셋팅해줍니다.
        self.livePhotoView.livePhoto = livePhoto
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // 이미지피커에서 선택을 취소했을 때 호출되는 메소드입니다.
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - LivePhotoCeollectionViewControllerDelegate
    // LivePhotoCeollectionViewController의 Delegate 메소드입니다.
    func livePhotoCollectionViewDidSelectedLivePhoto(asset: PHAsset) {
        
        // Live Photo를 가져올 때 사용할 요청 옵션입니다.
        let requestOption: PHLivePhotoRequestOptions = PHLivePhotoRequestOptions()
        // 네트워크 사용(iCloud 사진을 가져올 때 필요하므로)을 허용해줍니다.
        requestOption.networkAccessAllowed = true
        // 가장 최상의 퀄리티로 가져옵니다.
        requestOption.deliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat
        
        // 선택된 에셋에 해당하는 Live Photo를 가져옵니다.
        PHImageManager.defaultManager().requestLivePhotoForAsset(asset, targetSize: self.view.frame.size, contentMode: PHImageContentMode.AspectFit, options: requestOption, resultHandler: { (livePhoto: PHLivePhoto?, userInfo: [NSObject : AnyObject]?) -> Void in
            
            // 가져온 Live Photo를 화면에 셋팅합니다.
            self.livePhotoView.livePhoto = livePhoto
        })
    }
    
    // MARK: - Navigation
    // 스토리보드의 segue를 통해 화면이 전환되기 직전 호출됩니다.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // 다음 화면인 LivePhotoCollectionViewController의 델리게이트 객체를 현재 ViewController 객체로 지정합니다.
        if let livePhotoCollection = segue.destinationViewController as? LivePhotoCollectionViewController {
            livePhotoCollection.livePhotoCeollcetionViewControllerDelegate = self
        }
    }
}

