//
//  ViewController.swift
//  TextInpuNotification
//
//  Created by yagom on 2015. 8. 13..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // 화면에 입력받은 텍스트를 보여줄 레이블입니다.
    @IBOutlet weak var inputTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // AppDelegate에서 로컬 노티피케이션을 핸들링 하는 application:handleActionWithIdentifier:forLocalNotification 메서드에서 발송하는 노티피케이션을 수신등록합니다.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receiveInputNotification:", name: DidRecieveTextInputNotification, object: nil)
    }
    
    // DidRecieveTextInputNotification을 수신하는 메서드입니다.
    func receiveInputNotification(noti: NSNotification) {
        
        // 노티피케이션에 실려오는 userInfo가 있는지 확인합니다.
        if let userInfo = noti.userInfo {
            
            // userInfo 딕셔너리에 InputTextKey를 가진 값이 있는지 확인합니다.
            if let inputText = userInfo[InputTextKey] as? String {
                
                // 화면의 레이블에 텍스트를 설정해줍니다.
                self.inputTextLabel.text = inputText
            }
        }
    }
    
    // 로컬 노티피케이션을 발생시키는 메서드입니다.
    @IBAction func clickPostLocalNoti() {
        
        // 노티피케이션에서 텍스트를 입력받을 버튼에 해당하는 액션입니다.
        let userNotiInputAction: UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        // 액션 고유의 ID입니다.
        userNotiInputAction.identifier = "TextInput"
        // activationMode는 액션을 실행했을 때 애플리케이션이 실행될 상태를 지정합니다. Foreground로 설정하면 액션 실행시 애플리케이션이 실행됩니다.
        userNotiInputAction.activationMode = UIUserNotificationActivationMode.Background
        // 텍스트를 입력받는 액션으로 지정합니다.
        userNotiInputAction.behavior = UIUserNotificationActionBehavior.TextInput
        // 액션 버튼에 들어갈 제목입니다.
        userNotiInputAction.title = "텍스트 입력"
        
        // 노티피케이션에서 취소 버튼에 해당하는 액션입니다.
        let userNotiCancelAction: UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        userNotiCancelAction.identifier = "Cancel"
        userNotiCancelAction.activationMode = UIUserNotificationActivationMode.Background
        // 일반 액션으로 지정합니다.
        userNotiCancelAction.behavior = UIUserNotificationActionBehavior.Default
        userNotiCancelAction.title = "취소"
        // 일반 액션버튼과 구분하기 위하여 destructive로 설정해 줄 수 있습니다.
        userNotiCancelAction.destructive = true
        
        // 노티피케이션 카테고리를 지정해주고 위에서 만든 두 액션을 카테코리에 묶어줍니다.
        let notiCategory: UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
        notiCategory.identifier = "Test"
        notiCategory.setActions([userNotiInputAction, userNotiCancelAction], forContext: UIUserNotificationActionContext.Default)
        
        // 노티피케이션 타입을 설정합니다. 얼럿과 소리가 발생됩니다.
        let notiType: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Sound]
        
        // 노티피케이션 타입과 카테고리를 사용하여 노티피케이션 설정을 생성합니다.
        let notiSettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: notiType, categories: [notiCategory])
        
        // 노티피케이션 설정을 등록합니다.
        UIApplication.sharedApplication().registerUserNotificationSettings(notiSettings)
        
        // 로컬노티피케이션을 생성합니다.
        let localNoti: UILocalNotification = UILocalNotification()
        // 5초 뒤에 노티피케이션이 발생되도록 합니다.
        localNoti.fireDate = NSDate(timeIntervalSinceNow: 5)
        // 노티피케이션에 표시될 메세지입니다.
        localNoti.alertBody = "텍스트를 입력하세요"
        // 위에서 생성한 notiCategory 객체의 노티피케이션 카테고리와 일치시킵니다.
        localNoti.category = "Test"
        
        // 로컬 노티피케이션을 등록합니다.
        UIApplication.sharedApplication().scheduleLocalNotification(localNoti)
    }
}

