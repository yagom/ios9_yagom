//
//  AppDelegate.swift
//  TextInpuNotification
//
//  Created by yagom on 2015. 8. 13..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// 입력받은 텍스트를 다시 노티피케이션으로 발송해주는 노티피케이션 이름입니다.
let DidRecieveTextInputNotification = "DidReceiveTextInput"
// 입력받은 텍스트를 노티피케이션의 userInfo에 실어보낼 때 사용할 키입니다.
let InputTextKey = "InputText"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    // 로컬 노티피케이션을 핸들링하는 메서드입니다.
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, withResponseInfo responseInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        
        // 노티피케이션 액션의 ID를 확인합니다.
        if identifier == "TextInput" {
            
            // 입력된 텍스트가 있는지 확인합니다.
            if let inputText = responseInfo[UIUserNotificationActionResponseTypedTextKey] {
                
                // 입력된 텍스트를 노티피케이션의 userInfo에 실어 기본 노티피케이션 센터에 발송합니다. 발송한 노티피케이션은 ViewController의 receiveInputNotification: 메서드를 통해 수신하게 됩니다.
                NSNotificationCenter.defaultCenter().postNotificationName(DidRecieveTextInputNotification, object: nil, userInfo: [InputTextKey: inputText])
            }
        }
        
        // 로컬노티피케이션 처리가 완료되었음을 알리는 핸들러를 호출합니다.
        completionHandler()
    }
}

