//
//  AppDelegate.swift
//  ForceTouch
//
//  Created by yagom on 2015. 10. 22..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    var window: UIWindow?

    // 사용자가 퀵 액션을 선택하여 애플리케이션에 진입했을 때, 애플리케이션이 활성화 된 후 어떤 아이템이 선택되었는지 확인하기 위하여 임시저장할 속성입니다
    var launchedShortcutItem: UIApplicationShortcutItem?
    
    // 퀵 액션에 해당하는 아이템별로 실행해줄 내용을 핸들리하는 메소드입니다.
    func handleShortCutItem(shortcutItem: UIApplicationShortcutItem) -> Bool {
        var handled: Bool = false
        
        // 아이템의 타입이 정확한지 파악합니다
        guard ShortcutIdentifier(fullType: shortcutItem.type) != nil else { return false }
        
        // 타입을 String 형태로 가져옵니다.
        guard let shortCutType: String = shortcutItem.type as String? else { return false }
        
        switch (shortCutType) {
        case ShortcutIdentifier.First.type:
            print("Item 1 handling")
            handled = true
            break
        case ShortcutIdentifier.Second.type:
            print("Item 2 handling")
            handled = true
            break
        case ShortcutIdentifier.Third.type:
            print("Item 3 handling")
            handled = true
            break
        case ShortcutIdentifier.Fourth.type:
            print("Item 4 handling")
            handled = true
            break
        default:
            break
        }
        
        let alert: UIAlertController = UIAlertController(title: "알림", message: "\(shortcutItem.type)이 선택되었습니다.", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(okAction)
        self.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
        
        return handled
    }
    
    // MARK: Application delegate
    func applicationDidBecomeActive(application: UIApplication) {
        guard let shortcut = self.launchedShortcutItem else { return }
        self.handleShortCutItem(shortcut)
        self.launchedShortcutItem = nil
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // application(_:didFinishLaunchingWithOptions) 메소드는 반환값으로 추가적으로 핸들링이 필요한지의 여부를 Bool로 반환합니다.
        var shouldPerformAdditionalDelegateHandling: Bool = true
        
        // 퀵 액션을 통해 실행되면, UIApplicationLaunchOptionsShortcutItemKey키를 통하여 아이템 객체를 가져올 수 있습니다.
        if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsShortcutItemKey] as? UIApplicationShortcutItem {
            
            // 가져온 아이템을 할당해둡니다.
            self.launchedShortcutItem = shortcutItem
            
            // false를 반환하면 performActionForShortcutItem:completionHandler이 호출되지 않습니다. (추가적인 핸들링이 필요없다는 뜻이므로)
            shouldPerformAdditionalDelegateHandling = false
        }
        
        return shouldPerformAdditionalDelegateHandling
    }
    
    // 사용자가 퀵 액션을 통해 진입했을 때, application(_:,willFinishLaunchingWithOptions:) 또는 application(_:didFinishLaunchingWithOptions) 메소드가 false를 반환하지 않으면 실행됩니다.
    // Launch 관련 메소드는 최초 실행시 호출되므로 애플리케이션이 백그라운드 등에서 실행중에 퀵 액션을 선택하면 호출될 것입니다.
    func application(application: UIApplication, performActionForShortcutItem shortcutItem: UIApplicationShortcutItem, completionHandler: Bool -> Void) {
        
        let handledShortCutItem: Bool = handleShortCutItem(shortcutItem)
        completionHandler(handledShortCutItem)
    }

}

