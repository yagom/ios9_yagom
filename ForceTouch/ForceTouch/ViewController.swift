//
//  ViewController.swift
//  ForceTouch
//
//  Created by yagom on 2015. 10. 22..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// MARK: - ShortcutIdentifier
// 퀵 액션마다 지정될 ID의 enum입니다.
enum ShortcutIdentifier: String {
    case First
    case Second
    case Third
    case Fourth
    
    // MARK: Initializers
    // 초기화 메소드 - 번들ID를 사용해 유니크한 ID를 생성합니다.
    init?(fullType: String) {
        guard let last = fullType.componentsSeparatedByString(".").last else { return nil }
        
        self.init(rawValue: last)
    }
    
    // MARK: Properties
    // 번들ID를 사용해 type의 순수값(String)을 반환합니다.
    var type: String {
        return NSBundle.mainBundle().bundleIdentifier! + ".\(self.rawValue)"
    }
}
// MARK: - Shortcut UserInfo key
let AppShortcutUserInfoValueKey = "appShortcutUserInfoValueKey"

// MARK: - ViewController
class ViewController: UIViewController, UIViewControllerPreviewingDelegate {

    // MARK: Properties
    @IBOutlet var slider: UISlider!
    @IBOutlet var imageView: UIImageView!
    
    // MARK: Responder
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.slider.value = Float(touches.first!.force)
    }
    
    // MARK: IBActions
    // Add Shortcut items 버튼을 터치했을 때 실행될 메소드
    @IBAction func clickAddShortcutItemButton() {
        
        // 현재 애플리케이션의 싱글턴 객체를 가져옵니다
        let application: UIApplication = UIApplication.sharedApplication()
        
        // 만약 애플리케이션에 동적(dynamic) 퀵 액션 없다면 (UIApplication의 shortcutItems는 동적 퀵 액션만 반환합니다)
        if let shortcutItems = application.shortcutItems where shortcutItems.isEmpty {
            // 세 번째 동적 아이템과 네 번째 동적 아이템을 만들어줍니다.
            // subtitle과 userInfo는 선택사항이므로 넣어주어도 되고, 빼주어도 됩니다.
            let shortcut3 = UIApplicationShortcutItem(type: ShortcutIdentifier.Third.type, localizedTitle: "공유타입", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(type: UIApplicationShortcutIconType.Share), userInfo: [AppShortcutUserInfoValueKey: "value3"]
            )
            
            let shortcut4 = UIApplicationShortcutItem(type: ShortcutIdentifier.Fourth.type, localizedTitle: "추가타입", localizedSubtitle: "신규항목 추가", icon: UIApplicationShortcutIcon(type: UIApplicationShortcutIconType.Add), userInfo: [AppShortcutUserInfoValueKey: "value4"]
            )
            
            // 애플리케이션의 동적 퀵 액션 아이템을 갱신합니다.
            application.shortcutItems = [shortcut3, shortcut4]
        }
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ImageViewController에서 발송하는 PresentImageViewControllerNotification 노티피케이션을 수신할 것임을 등록합니다.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "presentImageViewController:", name: PresentImageViewControllerNotification, object: nil)
        
        // 3D Touch를 사용가능한지 체크합니다.
        if self.traitCollection.forceTouchCapability == UIForceTouchCapability.Available {
            // 미리보기(peek)의 델리게이트를 설정합니다.
            self.registerForPreviewingWithDelegate(self, sourceView: self.view)
        }
        else {
            // 사용자에게 3D Touch 사용불가를 알립니다.
            let alert: UIAlertController = UIAlertController(title: "알림", message: "3D Touch를 사용할 수 없습니다. 지원하지 않는 기기입니다.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "확인", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    // 뷰 컨트롤러가 메모리에서 해제될 때 호출됩니다.
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: UIViewControllerPreviewingDelegate
    // peek 기능을 통해 보여줄 미리보기 화면을 만들어 반환합니다.
    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        // 뷰 컨트롤러의 이미지뷰 안쪽으로 3D Touch가 감지되었을 때 동작합니다.
        if CGRectContainsPoint(self.imageView.frame, location) {
            
            // ImageViewController를 스토리보드ID를 통해 생성해줍니다.
            guard let imgViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ImageViewController") as? ImageViewController else { return nil }
            
            // 보여줄 이미지는 현재 뷰 컨트롤러의 이미지와 동일합니다.
            imgViewController.imageToShow = self.imageView.image
            
            // 미리보기 화면의 크기는 이미지의 크기로 정해봤습니다. 사이즈는 마음껏 조절해 보세요.
            if let size = self.imageView.image?.size {
                imgViewController.preferredContentSize = size
            }
            
            // peek이 되기까지 눌려지는 애니메이션의 기준으로 잡을 Rect입니다. sourceRect는 점점 선명해지고 주변부는 희미하게 블러됩니다.
            previewingContext.sourceRect = self.imageView.frame
            
            return imgViewController
        }
        return nil
    }
    
    // pop될 때 처리해줄 메소드입니다.
    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController) {
        // commitViewController는 peek화면에서 보여졌던 뷰 컨트롤러입니다. pop에서 재사용합니다.
        self.presentViewController(viewControllerToCommit, animated: true, completion: nil)
    }
    
    // MARK: Receive Notifications
    // PresentImageViewControllerNotification 노티피케이션을 수신했을 때 호출될 메소드입니다.
    // 이 경우에는 previewingContext 메소드를 사용할때와는 달리 뷰컨트롤러 재사용을 하지 않습니다.
    func presentImageViewController(noti: NSNotification) {
        
        guard let contentModeRawValue: Int = noti.userInfo?[KeyImageViewContentMode] as? Int else { return }
        guard let contentMode: UIViewContentMode = UIViewContentMode(rawValue: contentModeRawValue) else { return }
        
        guard let imgViewController: ImageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ImageViewController") as? ImageViewController else { return }
        
        imgViewController.imageToShow = self.imageView.image
        imgViewController.contentModeForImageView = contentMode
        
        self.presentViewController(imgViewController, animated: true, completion: nil)
    }
    
}