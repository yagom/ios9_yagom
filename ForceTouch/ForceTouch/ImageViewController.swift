//
//  ImageViewController.swift
//  ForceTouch
//
//  Created by yagom on 2015. 10. 23..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// MARK: - Notification Constants
// ImageViewController를 모달로 띄우고자 할 때 사용할 노티피케이션 이름입니다.
let PresentImageViewControllerNotification: String = "PresentImageView"
// PresentImageViewControllerNotification에서 userInfo 딕셔너리에 할당될 UIViewContentMode 옵션값의 키입니다.
let KeyImageViewContentMode: String = "ContentMode"

// MARK: - ImageViewController
class ImageViewController: UIViewController {
    
    // MARK: Properties
    
    // 화면의 이미지뷰에 보여질 이미지 객체입니다.
    var imageToShow: UIImage?
    // 화면의 이미지뷰의 ContentMode입니다.
    var contentModeForImageView: UIViewContentMode = UIViewContentMode.ScaleAspectFit
    
    @IBOutlet var imageView: UIImageView!
    
    // MARK: IBActions
    @IBAction func clickDoneButton() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.image = self.imageToShow
        self.imageView.contentMode = contentModeForImageView
    }
    
    // MARK: Preview Actions Items
    // 이 뷰컨트롤러의 미리보기 액션 아이템을 만들어줍니다.
    // 미리보기 액션 아이템은 peek 도중 화면을 위로 밀어올리면 나옵니다.
    override func previewActionItems() -> [UIPreviewActionItem] {
        // 아이템을 단일 액션으로 줄 수도 있습니다. - 닫기 액션
        let closeAction: UIPreviewAction = UIPreviewAction(title: "닫기", style: UIPreviewActionStyle.Destructive, handler: {
            (action: UIPreviewAction, viewController: UIViewController) -> Void in
            print("닫기 액션 핸들러")
        })
        
        // 이미지뷰의 contentMode를 ScaleAspectFill로 보여줄 액션입니다.
        // 액션을 선택하게 되면 미리보기 화면이 사라지게 되므로, 미리보기 화면이 사라진 후에 이미지 뷰가 해당 모드로 보여지도록 노티피케이션을 발송합니다.
        let contentModeScaleAspectFillAction: UIPreviewAction = UIPreviewAction(title: "ScaleAspectFill", style: UIPreviewActionStyle.Default) { (action: UIPreviewAction, viewController: UIViewController) -> Void in
            
            NSNotificationCenter.defaultCenter().postNotificationName(PresentImageViewControllerNotification,
                object: nil, userInfo: [KeyImageViewContentMode : UIViewContentMode.ScaleAspectFill.rawValue])
        }
        
        // 이미지뷰의 contentMode를 ScaleAspectFit으로 보여줄 액션입니다.
        let contentModeScaleAspectFitAction: UIPreviewAction = UIPreviewAction(title: "ScaleAspectFit", style: UIPreviewActionStyle.Default) { (action: UIPreviewAction, viewController: UIViewController) -> Void in
            
            NSNotificationCenter.defaultCenter().postNotificationName(PresentImageViewControllerNotification,
                object: nil, userInfo: [KeyImageViewContentMode : UIViewContentMode.ScaleAspectFit.rawValue])
        }
        
        // 이미지뷰의 contentMode를 ScaleToFill로 보여줄 액션입니다.
        let contentModeScaleToFillAction: UIPreviewAction = UIPreviewAction(title: "ScaleToFill", style: UIPreviewActionStyle.Default) { (action: UIPreviewAction, viewController: UIViewController) -> Void in
            
            NSNotificationCenter.defaultCenter().postNotificationName(PresentImageViewControllerNotification,
                object: nil, userInfo: [KeyImageViewContentMode : UIViewContentMode.ScaleToFill.rawValue])
        }
        
        // 아이템을 그룹 액션으로 줄 수도 있습니다. - [contentModeScaleAspectFillAction, contentModeScaleAspectFitAction, contentModeScaleToFillAction]
        let groupAction = UIPreviewActionGroup(title: "\(UIViewContentMode.self)", style: UIPreviewActionStyle.Default, actions: [contentModeScaleAspectFillAction, contentModeScaleAspectFitAction, contentModeScaleToFillAction])
        
        // 단일액션(닫기액션) 및 그룹액션을 반환합니다.
        return [closeAction, groupAction]
    }
}
