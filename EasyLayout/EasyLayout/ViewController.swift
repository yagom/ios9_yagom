//
//  ViewController.swift
//  EasyLayout
//
//  Created by yagom on 2015. 8. 13..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// 버튼이 클릭된 횟수를 체크하기 위한 전역변수입니다.
var clickCount = 0

class ViewController: UIViewController {
    
    // 스토리보드를 통해 화면에 표현될 버튼객체입니다.
    @IBOutlet weak var button: UIButton!
    
    // 버튼을 클릭했을 때 호출될 메서드입니다.
    @IBAction func clickButton(sender: UIButton) {
        
        // 버튼과 관련된 기존의 제약사항들을 제거해줍니다.
        for const: NSLayoutConstraint in self.view.constraints {
            if const.firstItem === self.button || const.secondItem === self.button {
                NSLayoutConstraint.deactivateConstraints([const])
            }
        }
        
        // 버튼의 상단 제약으로 쓰입니다.
        let topConst: NSLayoutConstraint
        // 버튼의 하단 제약으로 쓰입니다.
        let bottomConst: NSLayoutConstraint
        // 버튼의 리딩(왼쪽 시작) 제약으로 쓰입니다.
        let leadConst: NSLayoutConstraint
        // 버튼의 트레일링(오른쪽 끝) 제약으로 쓰입니다.
        let trailConst: NSLayoutConstraint
        
        // 버튼을 클릭할 때마다 늘어나는 clickCount를 가지고 나머지 연산을 한 결과를 통해 if문이 동작합니다.
        if clickCount % 6 == 0 {    // 버튼이 화면의 왼쪽 반을 채우게 됩니다.
            
            topConst =      self.button.topAnchor.constraintEqualToAnchor(self.topLayoutGuide.bottomAnchor)
            bottomConst =   self.button.bottomAnchor.constraintEqualToAnchor(self.bottomLayoutGuide.topAnchor)
            leadConst =     self.button.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
            trailConst =    self.button.trailingAnchor.constraintEqualToAnchor(self.view.centerXAnchor)
            
        } else if clickCount % 5 == 1 { // 버튼이 화면의 오른쪽 반을 채우게 됩니다.
            
            topConst =      self.button.topAnchor.constraintEqualToAnchor(self.topLayoutGuide.bottomAnchor)
            bottomConst =   self.button.bottomAnchor.constraintEqualToAnchor(self.bottomLayoutGuide.topAnchor)
            leadConst =     self.button.leadingAnchor.constraintEqualToAnchor(self.view.centerXAnchor)
            trailConst =    self.button.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
            
        } else if clickCount % 5 == 2 { // 버튼이 화면의 위 반을 채우게 됩니다.
            
            topConst =      self.button.topAnchor.constraintEqualToAnchor(self.topLayoutGuide.bottomAnchor)
            bottomConst =   self.button.bottomAnchor.constraintEqualToAnchor(self.view.centerYAnchor)
            leadConst =     self.button.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
            trailConst =    self.button.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
            
        } else if clickCount % 5 == 3 { // 버튼이 화면의 아래 반을 채우게 됩니다.
            
            topConst =      self.button.topAnchor.constraintEqualToAnchor(self.view.centerYAnchor)
            bottomConst =   self.button.bottomAnchor.constraintEqualToAnchor(self.bottomLayoutGuide.topAnchor)
            leadConst =     self.button.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
            trailConst =    self.button.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
            
        } else if clickCount % 5 == 4 { // 버튼이 상하좌우 50씩 여백을 가지게 됩니다.
            
            topConst =      self.button.topAnchor.constraintEqualToAnchor(self.topLayoutGuide.bottomAnchor, constant: 50.0)
            bottomConst =   self.button.bottomAnchor.constraintEqualToAnchor(self.bottomLayoutGuide.topAnchor, constant: -50.0)
            leadConst =     self.button.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor, constant: 50.0)
            trailConst =    self.button.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor, constant: -50.0)
            
        } else {    // 버튼이 화면을 꽉 채우게 됩니다.
            
            topConst =      self.button.topAnchor.constraintEqualToAnchor(self.topLayoutGuide.bottomAnchor)
            bottomConst =   self.button.bottomAnchor.constraintEqualToAnchor(self.bottomLayoutGuide.topAnchor)
            leadConst =     self.button.leadingAnchor.constraintEqualToAnchor(self.view.leadingAnchor)
            trailConst =    self.button.trailingAnchor.constraintEqualToAnchor(self.view.trailingAnchor)
            
        }
        
        // 새로운 제약사항들을 추가해줍니다.
        NSLayoutConstraint.activateConstraints([topConst, bottomConst, leadConst, trailConst])
        
        // 버튼을 클릭할 때마다 카운트를 증가시켜줍니다.
        clickCount++
    }
}

