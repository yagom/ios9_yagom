//
//  ViewController.swift
//  BasicContacts
//
//  Created by yagom on 2015. 7. 25..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// Contacts 프레임워크와 ContactsUI 프레임워크를 import 합니다.
import Contacts
import ContactsUI

// CNContactPickerDelegate 역할을 수행할 것을 명시합니다.
class ViewController: UIViewController, CNContactPickerDelegate {

    // MARK: - Properties
    // 선택된 연락처 정보를 가질 객체입니다.
    var selectedContact: CNContact?
    
    // MARK: - Methods
    
    func dismissModalViewController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - IBActions
    @IBAction func clickShowContactsPickerController() {

        // 연락처 선택 뷰 컨트롤러를 생성하여 화면에 보여줍니다.
        let contactsPicker: CNContactPickerViewController = CNContactPickerViewController()
        contactsPicker.delegate = self
        self.presentViewController(contactsPicker, animated: true, completion: nil)
    }
    
    @IBAction func clickShowContactsController() {
        
        // 선택해서 가져온 연락처 정보가 있다면 연락처 정보를 보여줍니다.
        if let contact = self.selectedContact {
            let contactViewController: CNContactViewController = CNContactViewController(forContact: contact)
            let navigation = UINavigationController(rootViewController: contactViewController)
            let closeButton = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.Done, target: self, action: "dismissModalViewController")
            self.presentViewController(navigation, animated: true, completion: { () -> Void in
                contactViewController.navigationItem.leftBarButtonItem = closeButton
            })
        }
    }
    
    @IBAction func clickEditContacts() {

        // 선택된 연락처 정보를 수정하는한 Mutable 객체로 복사해옵니다.
        if let mutableContact = self.selectedContact?.mutableCopy() as? CNMutableContact {
            
            // 이름을 변경할 수 있습니다.
            mutableContact.givenName = "yagom"
            mutableContact.familyName = "Jo"
            
            // 이메일 정보를 수정할 수 있습니다.
            let email = CNLabeledValue(label:CNLabelWork, value:"yagomsoft@gmail.com")
            let iCloudMail = CNLabeledValue(label:CNLabelEmailiCloud, value:"yagomsoft@me.com")
            mutableContact.emailAddresses = [email, iCloudMail]
            
            mutableContact.phoneNumbers = [CNLabeledValue(
                label:CNLabelPhoneNumberiPhone,
                value:CNPhoneNumber(stringValue:"010-0000-0000"))]
            
            // 홈페이지나 블로그 등 URL 정보를 변경할 수 있습니다.
            let blogURLString: String = "http://blog.yagom.net"
            mutableContact.urlAddresses = [CNLabeledValue(label: CNLabelURLAddressHomePage, value: blogURLString)]
            
            // 주소를 변경할 수 있습니다.
            let homeAddress = CNMutablePostalAddress()
            homeAddress.street = "Sang-su"
            homeAddress.city = "Mapo"
            homeAddress.state = "Seoul"
            homeAddress.postalCode = "00000"
            mutableContact.postalAddresses = [CNLabeledValue(label:CNLabelHome, value:homeAddress)]
            
            // 생일을 변경할 수 있습니다.
            let birthday = NSDateComponents()
            birthday.day = 11
            birthday.month = 7
            
            // 년도는 비밀이니까 넣지 않겠습니다
//            birthday.year = ???
            
            mutableContact.birthday = birthday
            
            // 연락처 정보를 저장하거나 가져올 수 있는 CNContactStore 객체입니다.
            let store = CNContactStore()
            
            // 저장 또는 변경을 위한 요청 객체입니다.
            let updateRequest = CNSaveRequest()
            updateRequest.updateContact(mutableContact)

            // 저장(기존 정보 업데이트)을 시도해보고 실패하면 로그를 남깁니다.
            do {
                try store.executeSaveRequest(updateRequest)
                self.selectedContact = mutableContact
            } catch let error {
                print(error)
                return
            }
        }
    }

    
    // MARK: - ContactsPickerViewController Delegate
    
    // 주소록에서 하나의 연락처 정보를 선택했을 때 연락처 객체가 넘어옵니다.
    // contactPicker:didSelectContacts 메소드와 동시에 구현할 경우 didSelectContacts 메소드가 우선됩니다.
    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact) {
        self.selectedContact = contact
    }
    /*
    // 예제에서 다루는 것 외에 다양한 델리게이트 메소드를 소개하기 위해 주석처리 합니다.
    // 주소록에서 여러개의 연락처 정보를 선택했을 때 연락처 객체의 배열이 넘어옵니다.
    func contactPicker(picker: CNContactPickerViewController, didSelectContacts contacts: [CNContact]) {
        
    }
    
    // 주소록의 연락처에서 하나의 속성을 선택했을 때 속성 객체가 넘어옵니다.
    // contactPicker:didSelectContactProperties 메소드와 동시에 구현할 경우 didSelectContactProperties 메소드가 우선됩니다.
    func contactPicker(picker: CNContactPickerViewController, didSelectContactProperty contactProperty: CNContactProperty) {
        
    }
    
    // 주소록의 연락처에서 여러개의 속성을 선택했을 때 속성 객체의 배열이 넘어옵니다.
    func contactPicker(picker: CNContactPickerViewController, didSelectContactProperties contactProperties: [CNContactProperty]) {
        
    }
    */
}

