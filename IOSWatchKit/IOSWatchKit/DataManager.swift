//
//  DataManager.swift
//  IOSWatchKit
//
//  Created by yagom on 2015. 9. 6..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// App Group Container ID입니다.
let KeySharedUserDefault = "group.net.yagom.WatchKit"

// User Defaults에 저장될 배열의 키입니다.
let KeyInfoArray: String = "InfoArr"

// 기존 NSUserDefaults 클래스에 extension을 통해 메소드를 추가해줍니다.
extension NSUserDefaults {
    
    // NSUserDefaults에서 배열을 가져옵니다.
    // 만난 시각들을 저장하고 있는 배열입니다.
    func infoArray(inout arr: [NSDate]?) {
        
        // 기존에 배열이 존재한다면
        if let infoArr = self.objectForKey(KeyInfoArray) as? [NSDate] {
            arr = infoArr
        } else {
            // 그렇지 않다면 새로 만들어줍니다.
            arr = [NSDate]()
            
            if let realArr = arr {
                self.setObject(realArr, forKey: KeyInfoArray)
            }
            self.synchronize()
        }
    }
    
    // 만난 시각을 추가합니다.
    func addNewDate(inout arr: [NSDate]?) {
        arr?.append(NSDate())
        self.setObject(arr, forKey: KeyInfoArray)
        self.synchronize()
    }
}

// App Group Container ID를 사용하여 워치와 공유하는 User Defaults 객체를 불러오는 전역함수입니다.
func mySharedUserDefault() -> NSUserDefaults? {
    let shardDefault: NSUserDefaults? = NSUserDefaults(suiteName: KeySharedUserDefault)
    return shardDefault
}