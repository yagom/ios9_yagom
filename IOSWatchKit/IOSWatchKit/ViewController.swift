//
//  ViewController.swift
//  IOSWatchKit
//
//  Created by yagom on 2015. 9. 6..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// 시간을 리스트로 표현하고 싶기 때문에 테이블뷰 컨트롤러를 사용하였습니다.
class ViewController: UITableViewController {
    
    // MARK:- Properties
    var sharedUserDefault: NSUserDefaults?
    var infoArr: [NSDate]?
    var dateFormatter: NSDateFormatter?
    
    // MARK:- IBAction
    @IBAction func clickPlusButton() {
        self.sharedUserDefault?.addNewDate(&self.infoArr)
        
        self.refreshView()
    }
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 날짜 포멧을 만들어줍니다.
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter?.dateFormat = "yyyy.MM.dd - hh:mm:ss"
        self.dateFormatter?.locale = NSLocale.currentLocale()
        
        self.refreshView()
    }
    
    // MARK:- Refresh
    
    func refreshView() {
        
        // App Groups의 공용 UserDefault를 가져오고, 배열도 가져옵니다.
        self.sharedUserDefault = mySharedUserDefault()
        self.sharedUserDefault?.infoArray(&self.infoArr)
        
        // 뷰 컨트롤러의 제목을 지정해줍니다.
        if let count = self.infoArr?.count {
            self.title = "Count : \(count)"
        }
        
        self.tableView?.reloadData()
    }
    
    // MARK:- TableView Data Source
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // 만난 시각을 저장한 배열을 가져와 개수를 세고, 테이블뷰의 row수로 반환합니다.
        if let count = self.infoArr?.count {
            return count
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // 스토리보드에 미리 만들어둔 셀을 가져옵니다.
        let cell = tableView.dequeueReusableCellWithIdentifier("InfoCell")
        
        // NSDate 객체를 NSDateFormatter를 사용하여 String으로 변환하여 셀의 텍스트로 세팅해줍니다.
        if let date: NSDate = infoArr?[indexPath.row] {
            cell?.textLabel?.text = self.dateFormatter?.stringFromDate(date)
        }
        
        return cell!
    }
}
