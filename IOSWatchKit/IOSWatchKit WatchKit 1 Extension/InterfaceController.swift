//
//  InterfaceController.swift
//  IOSWatchKit WatchKit 1 Extension
//
//  Created by yagom on 2015. 9. 6..
//  Copyright © 2015년 yagom. All rights reserved.
//

import WatchKit
import Foundation

// WKInterfaceTable의 Row를 표현해줄 컨트롤러입니다.
class RowController: NSObject {
    @IBOutlet weak var rowLabel: WKInterfaceLabel?
}

// 워치 애플리케이션의 메인 화면이 될 클래스입니다.
class InterfaceController: WKInterfaceController {
    
    // MARK:- Properties
    var sharedUserDefault: NSUserDefaults?
    var infoArr: [NSDate]?
    var dateFormatter: NSDateFormatter?
    
    @IBOutlet var countLabel: WKInterfaceLabel?
    @IBOutlet var table: WKInterfaceTable?
    
    // MARK:- IBActions
    @IBAction func clickAddDateButton() {
        self.sharedUserDefault?.addNewDate(&self.infoArr)
        self.setViewData()
    }
    
    // Force Touch 메뉴 중 첫 번째 메뉴 선택 시 호출되는 메소드입니다.
    @IBAction func doMenuOne(sender: AnyObject?) {
        print("select menu 1")
    }
    
    // Force Touch 메뉴 중 두 번째 메뉴 선택 시 호출되는 메소드입니다.
    @IBAction func doMenuTwo(sender: AnyObject?) {
        print("select menu 2")
    }
    
    // MARK:- Life Cycle
    
    // UIViewControlelr의 awakeFromNib과 유사한 메소드입니다.
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // 날짜 형식은 호스트 애플리케이션 보다는 조금 간략하게 표현해 줍니다.
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter?.dateFormat = "yy.MM.dd - hh:mm"
        self.dateFormatter?.locale = NSLocale.currentLocale()
    }
    
    // 애플리케이션이 활성화 되기 직전에 호출됩니다.
    override func willActivate() {
        super.willActivate()
        self.setViewData()
    }
    
    // 애플리케이션이 비활성화 된 후 호출됩니다.
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    // MARK:- Setting Views
    // 화면에 보여질 데이터를 처리하고 세팅합니다.
    private func setViewData() {
        
        self.sharedUserDefault = mySharedUserDefault()
        self.sharedUserDefault?.infoArray(&self.infoArr)
        
        if let arr = self.infoArr {
            let count: Int = arr.count
            
            self.countLabel?.setText("Count : \(count)")
            
            if count == 0 {return}
            
            // WatchKit의 테이블은 델리게이트를 사용하지 않고 직접 데이터를 세팅해줍니다.
            self.table?.setNumberOfRows(count, withRowType: "RowController")
            
            for var i = 1; i < count + 1 ; i++ {
                if let date: NSDate = arr[count - i] {
                    if let str = self.dateFormatter?.stringFromDate(date) {
                        let row = self.table?.rowControllerAtIndex(i-1) as? RowController
                        row?.rowLabel?.setText(str)
                    }
                }
            }
        }
    }
}
