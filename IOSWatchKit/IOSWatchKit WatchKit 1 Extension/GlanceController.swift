//
//  GlanceController.swift
//  IOSWatchKit WatchKit 1 Extension
//
//  Created by yagom on 2015. 9. 6..
//  Copyright © 2015년 yagom. All rights reserved.
//

import WatchKit
import Foundation

// 워치 애플리케이션의 한 눈에 보기 화면이 될 클래스입니다.
class GlanceController: WKInterfaceController {
    
    // MARK:- Properties
    var sharedUserDefault: NSUserDefaults?
    var infoArr: [NSDate]?
    var dateFormatter: NSDateFormatter?
    
    @IBOutlet var countLabel: WKInterfaceLabel?
    @IBOutlet var table: WKInterfaceTable?
    
    // MARK:- Life Cycle
    // UIViewControlelr의 awakeFromNib과 유사한 메소드입니다.
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter?.dateFormat = "yy.MM.dd - hh:mm"
        self.dateFormatter?.locale = NSLocale.currentLocale()
        // Configure interface objects here.
    }
    
    // 한 눈에 보기 화면이 활성화 되기 직전에 호출됩니다.
    override func willActivate() {
        super.willActivate()
        
        self.sharedUserDefault = mySharedUserDefault()
        self.sharedUserDefault?.infoArray(&self.infoArr)
        
        if let arr = self.infoArr {
            let count: Int = arr.count
            
            self.countLabel?.setText("Count : \(count)")
            
            if count == 0 {return}
            
            // WatchKit의 테이블은 델리게이트를 사용하지 않고 직접 데이터를 세팅해줍니다.
            self.table?.setNumberOfRows(2, withRowType: "RowController")
            
            for var i = 1; i < (count > 2 ? 3 : count) ; i++ {
                if let date: NSDate = arr[count - i] {
                    if let str = self.dateFormatter?.stringFromDate(date) {
                        let row = self.table?.rowControllerAtIndex(i-1) as? RowController
                        row?.rowLabel?.setText(str)
                    }
                }
            }
        }
    }
    
    // 한 눈에 보기 화면이 비활성화 된 후 호출됩니다.
    override func didDeactivate() {
        super.didDeactivate()
    }
}
