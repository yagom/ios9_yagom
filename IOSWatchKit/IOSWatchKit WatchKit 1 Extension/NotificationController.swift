//
//  NotificationController.swift
//  IOSWatchKit WatchKit 1 Extension
//
//  Created by yagom on 2015. 9. 6..
//  Copyright © 2015년 yagom. All rights reserved.
//

import WatchKit
import Foundation

// 노티피케이션 화면을 컨트롤 할 클래스입니다.
class NotificationController: WKUserNotificationInterfaceController {
    
    // MARK:- Properties
    @IBOutlet var titleLabel: WKInterfaceLabel?
    @IBOutlet var messageLabel: WKInterfaceLabel?
    
    // MARK:- Life Cycle
    override init() {
        super.init()
    }
    
    // 노티피케이션 화면이 활성화 되기 전 호출됩니다.
    override func willActivate() {
        super.willActivate()
    }
    
    // 노티피케이션 화면이 비활성화 된 후 호출됩니다.
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    // MARK:- Receive Remote Notificaiton
    override func didReceiveRemoteNotification(remoteNotification: [NSObject : AnyObject], withCompletion completionHandler: ((WKUserNotificationInterfaceType) -> Void)) {
        
        // 노티피케이션의 페이로드에 customKey라는 키를 가진 값이 있는지 확인해 봅니다.
        if let customMessage = remoteNotification["customKey"] as? String {
            
            // 스토리보드의 커스텀 화면으로 구성된 메세지 레이블에 커스텀 메세지를 세팅합니다.
            self.messageLabel?.setText(customMessage)
            
            // aps 객체에서 필요한 정보를 가져와 제목 레이블에 셋팅합니다.
            if let aps = remoteNotification["aps"] as? [NSObject : AnyObject],
                alert = aps["alert"] as? [NSObject : AnyObject],
                title = alert["title"] as? String {
                    
                    self.titleLabel?.setText(title)
            }
            
            // 커스텀 노티피케이션 화면을 사용할 것을 핸들러를 통해 알립니다.
            completionHandler(WKUserNotificationInterfaceType.Custom)
            return
        }
        
        // customKey가 없다면 기본 인터페이스를 사용한 노티피케이션 화면을 보여줍니다.
        completionHandler(WKUserNotificationInterfaceType.Default)
    }
    
}
