//
//  ViewController.swift
//  CustomSearch
//
//  Created by yagom on 2015. 8. 24..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import CoreSpotlight
import MobileCoreServices


// 영웅의 정보를 담을 구조체입니다.
struct Hero {
    var name: String
    var weapon: String
    var birth: NSDate
}

func searchableItemAttributeSetWithHero(hero: Hero) -> CSSearchableItemAttributeSet {
    
    // 영웅의 생년월일을 문자열로 상호 변환할 때 사용합니다.
    let dateFormatter: NSDateFormatter = NSDateFormatter()
    dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
    
    let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeItem as String)
    
    attributeSet.title = hero.name
    attributeSet.contentDescription = dateFormatter.stringFromDate(hero.birth) + "출생" + "\n" + hero.weapon + " 사용"
    
    let imgURL = NSBundle.mainBundle().URLForResource("flag", withExtension: "png")
    
    if let url = imgURL {
        if let img = NSData(contentsOfURL: url) {
            attributeSet.thumbnailData = img
        }
    }
    
    // 검색에 사용될 키워드입니다.
    let keywords: [String] = [hero.name, hero.weapon, "영웅"]
    attributeSet.keywords = keywords
    
    return attributeSet
}

class ViewController: UITableViewController {

    // MARK:- Properties
    
    
    // User Activity를 통해 진입했을 때 복원할 화면을 위한 변수입니다.
    var heroToRestore: Hero? {
        didSet {
            if heroToRestore != nil {
                self.navigationController?.popToRootViewControllerAnimated(false)
                self.performSegueWithIdentifier("showHero", sender: self)
            }
        }
    }
    
    // 영웅 배열
    lazy var heros: [Hero] = [
        Hero(name: "윤봉길", weapon: "도시락 폭탄", birth: NSDate(timeIntervalSince1970: -1941753600)),
        Hero(name: "안중근", weapon: "권총", birth: NSDate(timeIntervalSince1970: -2850595200)),
        Hero(name: "이봉창", weapon: "수류탄", birth: NSDate(timeIntervalSince1970: -2189894400)) ]
    
    // MARK:- Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 기존에 검색가능한 아이템들을 삭제합니다.
        CSSearchableIndex.defaultSearchableIndex().deleteSearchableItemsWithDomainIdentifiers(["heros"]) { (error) -> Void in
            if let err = error {
                print(err.localizedDescription)
            }
        }
        
        var searchableItems: [CSSearchableItem] = []
        
        // 영웅들을 검색 가능하도록 Searchable Item으로 추가합니다.
        for hero in self.heros {
            
            // 검색 가능한 아이템의 속성 셋 생성
            let attributeSet = searchableItemAttributeSetWithHero(hero)
            
            // 속성 셋을 통하여 검색 가능한 아이템을 만듭니다.
            let item = CSSearchableItem(uniqueIdentifier: hero.name, domainIdentifier: "heros", attributeSet: attributeSet)
            searchableItems.append(item)
        }
        
        // 검색 가능한 아이템들을 색인등록 합니다.
        CSSearchableIndex.defaultSearchableIndex().indexSearchableItems(searchableItems) { (error) -> Void in
            if let err = error {
                print(err.localizedDescription)
            } else {
                print("searhable items indexed")
            }
        }
    }
    
    // MARK:- Segues
    // 스토리보드의 Segue를 통해 화면이 이동하기 전에 호출되는 메소드입니다.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // showHero segue인지 확인합니다.
        if segue.identifier == "showHero" {
            
            var heroToShow: Hero? = nil
            
            // 테이블뷰의 항목을 선택했다면 여기에서 heroToShow가 셋팅됩니다.
            if let indexPath = self.tableView.indexPathForSelectedRow {
                heroToShow = heros[indexPath.row]
            }
            else if let hero = self.heroToRestore { // User Activity를 통해 들어왔을 땐 여기서 셋팅됩니다.
                heroToShow = hero
                self.heroToRestore = nil
            }

            if let hero =  heroToShow {
                // 보여질 DetailViewController의 hero 속성을 셋팅해줍니다.
                let controller = segue.destinationViewController as! DetailViewController
                controller.hero = hero
            }
        }
    }
    
    // MARK: - Table View
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.heros.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        // 각 테이블뷰 셀에 이름을 셋팅해줍니다.
        let hero: Hero = heros[indexPath.row]
        cell.textLabel?.text = hero.name
        
        return cell
    }
    

    // MARK:- Restore User Activity
    // User Activity의 복원시도를 때 호출되는 메소드입니다.
    override func restoreUserActivityState(activity: NSUserActivity) {

        // kCSSearchableItemActivityIdentifier이라는 키로 아이템 title이 들어옵니다.
        if let name = activity.userInfo?["kCSSearchableItemActivityIdentifier"] as? String {
            
            // 복원될 인물정보를 heroToRestore에 셋팅해줍니다.
            if let found = self.heros.filter( {$0.name == name} ).first {
                self.heroToRestore = found
            }
        } else {
            let alert = UIAlertController(title: "알림", message: "우리의 영웅을 찾을 수가 없습니다.\n\(activity.userInfo)", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "확인", style: UIAlertActionStyle.Cancel, handler: nil))
            
            self.presentViewController(alert, animated: false, completion: nil)
        }
    }
}

