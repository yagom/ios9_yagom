//
//  AppDelegate.swift
//  CustomSearch
//
//  Created by yagom on 2015. 8. 24..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }

    // UserActivity를 통해 진입했을 때 실행될 델리게이트 메소드입니다.
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        let navigationController: UINavigationController = self.window?.rootViewController as! UINavigationController
        navigationController.popToRootViewControllerAnimated(false)
        navigationController.topViewController?.restoreUserActivityState(userActivity)
        return true
    }
}

