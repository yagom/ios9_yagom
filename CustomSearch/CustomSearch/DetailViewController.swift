//
//  DetailViewController.swift
//  CustomSearch
//
//  Created by yagom on 2015. 8. 24..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit
import CoreSpotlight

class DetailViewController: UIViewController {
    
    // MARK:- Properties
    // 스토리보드의 객체와 연결될 아울렛 변수들입니다.
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weaponLabel: UILabel!
    @IBOutlet weak var birthLabel: UILabel!
    
    // 화면에 보여질 인물정보를 담을 변수입니다.
    var hero: Hero?
    
    // MARK:- Life Cycle
    // 화면이 보여지기 직전 호출됩니다.
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        if let object = self.hero {

            // 화면에 적절한 텍스트를 셋팅합니다.
            self.nameLabel.text = object.name
            self.weaponLabel.text = object.weapon
            self.birthLabel.text = dateFormatter.stringFromDate(object.birth)
            
            // 화면에 진입했다는 User Activity를 생성합니다.
            // activityType에 들어가는 문자열은 Info.plist에 생성해둔 NSUserActivityTypes의 아이템 항목과 일치해야 합니다.
            let activity = NSUserActivity(activityType: "net.yagom.CustomSearch.showContent")
            activity.userInfo = ["name": object.name, "weapon": object.weapon, "birth": object.birth]
            activity.title = object.name

            // 검색에 사용될 키워드입니다.
            let keywords: Set<String> = [object.name, object.weapon, "영웅"]
            activity.keywords = keywords
            
            // 핸드오프 기능을 위한 User Activity는 아님을 뜻합니다.
            activity.eligibleForHandoff = false
            
            // 기기 내 검색을 위한 User Activity임을 알려줍니다.
            activity.eligibleForSearch = true
            
            // 액티비티 검색 결과를 이미지를 포함해서 보고싶으면 속성 셋을 통하여 검색 가능한 아이템을 만들어 포함시켜줍니다.
            let attributeSet: CSSearchableItemAttributeSet = searchableItemAttributeSetWithHero(object)
            activity.contentAttributeSet = attributeSet
            
            // 활성화 합니다.
            activity.becomeCurrent()
        }
    }
}
