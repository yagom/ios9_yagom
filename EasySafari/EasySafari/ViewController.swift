//
//  ViewController.swift
//  EasySafari
//
//  Created by yagom on 2015. 7. 25..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

// 더욱 강력해진 SafariServices 프레임워크를 import 합니다.
import SafariServices

// ViewController객체가 SafariViewControllerDelegate의 역할을 수행할 것을 명시합니다.
class ViewController: UIViewController, SFSafariViewControllerDelegate {

    // MARK: - IBActions
    @IBAction func clickShowSafariViewController() {
        
        // NSURL 객체 생성에 실패할 확률이 있으므로 if-let 구문을 사용하여 nil이 반환되지 않는지 확인합니다.
        if let url = NSURL(string: "http://blog.yagom.net") {
            
            // Safari 뷰컨트롤러를 원하는 URL을 사용하여 생성하고 화면에 보여줍니다.
            let safariViewController = SFSafariViewController(URL: url)
            safariViewController.delegate = self
            self.presentViewController(safariViewController, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - SFSafariViewControllerDelegate
    
    // 사파리 뷰 컨트롤러의 Done(완료) 버튼을 선택했을 때 호출되는 델리게이트 메소드입니다.
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        
        // 모달로 올라온 화면을 내려줍니다.
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // 또다른 SFSafariViewControllerDelegate 메소드입니다.
    // 특정 Activity를 특정 URL에 맞게 제공할 수 있습니다.
    func safariViewController(controller: SFSafariViewController, activityItemsForURL URL: NSURL, title: String?) -> [UIActivity] {
        
    }
    */

}

