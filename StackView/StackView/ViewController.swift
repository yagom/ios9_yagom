//
//  ViewController.swift
//  StackView
//
//  Created by yagom on 2015. 7. 27..
//  Copyright © 2015년 yagom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // 스토리보드와 연결될 세로 스택 뷰 아울렛변수입니다.
    @IBOutlet var verticalStackView: UIStackView!
    
    
    // 스토리보드와 연결될 가로 스택 뷰 아울렛변수입니다.
    @IBOutlet var horizontalStackView: UIStackView!
    
    // 임의의 수를 생성하여 임의의 이미지를 불러와 이미지뷰를 생성하여 반환하는 메소드입니다.
    func imageViewWithRandomImage() -> UIImageView {
        
        let randNum = rand() % 4
        let imageName = "yagom\(randNum)"
        let yagomImageView: UIImageView = UIImageView(image: UIImage(named: imageName))
        
        yagomImageView.contentMode = .ScaleAspectFit
        
        return yagomImageView
    }
    
    // Add Vertical Stack 버튼을 선택하였을 때 호출될 액션 메소드입니다.
    @IBAction func clickAddVerticalStackButton(sender: AnyObject) {
        
        // 임의의 새로운 이미지뷰를 생성하여
        let newImageView = imageViewWithRandomImage()
        
        // 세로 스택 뷰에 서브뷰를 추가합니다.
        // 중간의 특정 인덱스에 서브뷰를 삽입하고자 한다면 insertArrangedSubview 메소드를 사용할 수 있습니다.
        self.verticalStackView.addArrangedSubview(newImageView)
        
        // 기본 애니메이션과 함께 스택뷰의 레이아웃을 재정비합니다.
        UIView.animateWithDuration(0.25, animations: {
            self.verticalStackView.layoutIfNeeded()
        })
        
    }
    
    // Remove Vertical Stack 버튼을 선택하였을 때 호출될 액션 메소드입니다.
    @IBAction func clickRemoveVerticalStackButton(sender: AnyObject) {
        
        // 맨 마지막에 추가된 서브뷰를 불러와서
        if let lastImageView = self.verticalStackView.arrangedSubviews.last {
            
            // 스택뷰의 관리범위에서 꺼냅니다.
            self.verticalStackView.removeArrangedSubview(lastImageView)
            
            // 최종적으로 화면에서 제거합니다.
            lastImageView.removeFromSuperview()
            
            // 기본 애니메이션과 함께 스택뷰의 레이아웃을 재정비합니다.
            UIView.animateWithDuration(0.25, animations: {
                self.verticalStackView.layoutIfNeeded()
            })
        }
    }
    
    // Add Horizontal Stack 버튼을 선택하였을 때 호출될 액션 메소드입니다.
    // 각 코드는 clickAddVerticalStackButton 메소드와 유사합니다.
    @IBAction func clickAddHorizontalStackButton(sender: AnyObject) {
        
        let newImageView = imageViewWithRandomImage()
        
        self.horizontalStackView.addArrangedSubview(newImageView)
        
        UIView.animateWithDuration(0.25, animations: {
            self.horizontalStackView.layoutIfNeeded()
        })
    }
    
    // Remove Horizontal Stack 버튼을 선택하였을 때 호출될 액션 메소드입니다.
    // 각 코드는 clickRemoveVerticalStackButton 메소드와 유사합니다.
    @IBAction func clickRemoveHorizontalStackButton(sender: AnyObject) {
        
        if let lastImageView = self.horizontalStackView.arrangedSubviews.last {
            self.horizontalStackView.removeArrangedSubview(lastImageView)
            lastImageView.removeFromSuperview()
            UIView.animateWithDuration(0.25, animations: {
                self.horizontalStackView.layoutIfNeeded()
            })
        }
    }
}

